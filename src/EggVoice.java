import java.util.Objects;

public class EggVoice extends Thread {
    public int counter = 0;

    @Override
    public void run() {
        for (int i = 1; i <= 5; i++) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {

            }

            System.out.println("яйцо!");
            counter++;
            if (!Main.chicken.isAlive() & !Objects.equals(Main.correctClass, "Курица") & i==5) {
                Main.correctClass = "Яйцо";
            }
        }
    }

    @Override
    public String toString() {
        return EggVoice.currentThread().toString() + "; Счетчик - " + counter;
    }
}
