public class Main {
    public static String correctClass;
    static ChickenVoice chicken;
    static EggVoice egg;

    public static void main(String[] args) {
        chicken = new ChickenVoice();
        egg = new EggVoice();
        System.out.println("Начинаем спор...");
        chicken.start();
        egg.start();
        while (correctClass==null) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {

            }
        }
        System.out.println("Первым было - " + correctClass);
    }
}
