import java.util.Objects;

public class ChickenVoice extends Thread {
    public int counter = 0;

    @Override
    public void run() {
        for (int i = 1; i <= 5; i++) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {

            }

            System.out.println("Курица!" + Thread.currentThread().toString());
            counter++;
            if (!Main.egg.isAlive() & !Objects.equals(Main.correctClass, "Яйцо") & i == 5) {
                Main.correctClass = "Курица";
            }
        }
    }

    @Override
    public String toString() {
        return ChickenVoice.currentThread().toString() + "; Счетчик - " + counter;
    }
}
